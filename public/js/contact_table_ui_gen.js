var contact_table_ui_gen = function(parent_identifier, table_identifier) {

  this.parent_id=parent_identifier;
  this.table_id =table_identifier;
  
  this.exampleData=[["Tiger","Nixon","uxbridge lane","kingsway","Edinburgh","Gloucestershire","England","GL2 5GH","01452369963"],["Tiger","Loeb","uxbridge lane","kingsway","Edinburgh","Gloucestershire","England","GL2 5GH","01452369963"]]
  
  
  this.generate_table = function(header_names) {
    // First generate our table
	d3_table = d3.selectAll('#' + this.parent_id).append("table");
	d3_table.attr("id", this.table_id);
	d3_table.attr("class", "display");
	d3_table.attr("cellspacing", "0");
	d3_table.attr("width", "100%");

    d3_table_head = d3_table.append("thead");
    d3_table_head_row = d3_table_head.append("tr");
	// Append all of the table header column values to the table
	d3_table_head_row.selectAll("th").data(header_names).enter().append("th").text(function (text_value){return text_value;} );
	
	// now activate data tables
	jquery_table= jQuery('#' + this.table_id).dataTable({
		//serverSide: true,
		//ajax: {
		//	url: '/data-source',
		//	type: 'POST'
		//}
		  data: this.exampleData
	});
	
	jQuery('#' + this.table_id + ' tbody').on( 'click', 'tr', function () {
		if ( jQuery(this).hasClass('selected') ) {
            jQuery(this).removeClass('selected');
        }
        else {
			jquery_table.$('tr.selected').removeClass('selected');
            jQuery(this).addClass('selected');
        }
    } );
  };
};