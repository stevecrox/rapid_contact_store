var contact_modal_ui_gen = function(parent_identifier, model_identifier) {

    // this.data_format = [{"first_name","First Name","Joe"},];

    this.validator = new Validation();
    this.parent_id = parent_identifier;
    this.modal_id = model_identifier;

    this.get_parent = function(parent_identifier) {
        var parent;

        if (this.validator.isValidString(parent_identifier)) {
            parent = d3.selectAll('#' + parent_identifier);
        } else if (this.validator.isValidObject(parent_identifier)) {
            parent = parent_identifier;
        } else {
            parent = null;
        }

        return parent;
    }

    this.get_modal_parent_identifier = function() {
        return this.modal_id + "_parent";
    }

    this.create_modal_control_button = function(parent_identifier, modal_identifier, modal_text, btn_class) {
        // provide default values
        btn_class = typeof btn_class !== 'undefined' ? btn_class : "btn btn-primary";

        var parent = this.get_parent(parent_identifier);

        var d3_modal_btn = parent.append("input");
        d3_modal_btn.attr("id", modal_identifier);
        d3_modal_btn.attr("class", btn_class);
        d3_modal_btn.attr("type", "button");
        d3_modal_btn.attr("data-toggle", "modal");

        if (this.validator.isValidString(modal_text)) {
            d3_modal_btn.attr("value", modal_text);
            d3_modal_btn.text(modal_text);
        }
        return d3_modal_btn;
    };

    this.generate_modal = function(header_text, button_text) {

        var parent = this.get_parent(this.parent_id);

        var d3_modal_parent = parent.append("div");
        d3_modal_parent.attr("id", this.modal_id + "_parent");
        d3_modal_parent.attr("class", "modal fade");
        d3_modal_parent.attr("tabindex", "-1");
        d3_modal_parent.attr("role", "dialog");
        d3_modal_parent.attr("aria-labelledby", "myModalLabel");
        d3_modal_parent.attr("aria-hidden", "true");

        var d3_modal_dialog = d3_modal_parent.append("div");
        d3_modal_dialog.attr("id", this.modal_id + "_dialog");
        d3_modal_dialog.attr("class", "modal-dialog");

        var d3_modal_content = d3_modal_dialog.append("div");
        d3_modal_content.attr("id", this.modal_id + "_content");
        d3_modal_content.attr("class", "modal-content");

        var d3_modal_header = d3_modal_content.append("div");
        d3_modal_header.attr("id", this.modal_id + "_header");
        d3_modal_header.attr("class", "modal-header");

        var d3_close_btn = this.create_modal_control_button(d3_modal_header, this.modal_id + "_close_btn", "x", "btn");
        d3_close_btn.attr("data-dismiss", "modal");

        var d3_header_text = d3_modal_header.append("h4");
        d3_header_text.attr("class", "modal-title");
        d3_header_text.text(header_text);

        var d3_modal_body = d3_modal_content.append("div");
        d3_modal_body.attr("id", this.modal_id + "_body");
        d3_modal_body.attr("class", "modal-body");

        this.generate_contact_form();
        var d3_modal_footer = d3_modal_content.append("div");
        d3_modal_footer.attr("id", this.modal_id + "_footer");
        d3_modal_footer.attr("class", "modal-footer");

        d3_submit_btn = this.create_modal_control_button(d3_modal_footer, this.modal_id + "_submit_btn", button_text);
        d3_submit_btn.attr("data-dismiss", "modal");
    };

    this.generate_contact_form = function() {
        var d3_modal_body = this.get_parent(this.modal_id + "_body");

        var d3_form = d3_modal_body.append("form");
        d3_form.attr("id", this.modal_id + "_form");
        d3_form.attr("class", "form-horizontal");
        d3_form.attr("role", "form");

        var d3_fieldset = d3_form.append("fieldset");
        d3_fieldset.attr("id", this.modal_id + "_fieldset");

        this.create_form_group("first_name", "First Name", "Joe");
        this.create_form_group("surname", "Surname", "Bloggs");
        this.create_form_group("address_line_1", "Address", "Address Line 1");
        this.create_form_group("address_line_2", "", "Address Line 2");
        this.create_form_group("city", "City", "City");

        var d3_create_modal_dual_form = this
                .create_form_group("county_post_code", "County", "United Kingdom", "2", "4");
        this.append_form_group_specifiy_sizes(d3_create_modal_dual_form, "post_code", "Post Code", "UK4 IGB", "3", "3");

        this.create_form_group("telephone", "Telephone", "");

        return d3_fieldset;
    };

    this.append_form_group_specifiy_sizes = function(parent_identifier, machine_id, man_readable_id, placeholder,
            label_size, input_size) {
        label_size = typeof label_size !== 'undefined' ? label_size : "2";
        input_size = typeof input_size !== 'undefined' ? input_size : "10";

        var d3_parent = this.get_parent(parent_identifier);

        var d3_form_label = d3_parent.append("label");
        d3_form_label.attr("id", machine_id + "_label");
        d3_form_label.attr("class", "col-xs-" + label_size + " col-sm-" + label_size + " col-md-" + label_size
                + " col-lg-" + label_size + " control-label");
        d3_form_label.attr("for", "textinput");
        d3_form_label.text(man_readable_id);

        var d3_form_input_wrapper = d3_parent.append("div");
        d3_form_input_wrapper.attr("id", machine_id + "_form_input-wrapper");
        d3_form_input_wrapper.attr("class", "col-xs-" + input_size + " col-sm-" + input_size + " col-md-" + input_size
                + " col-lg-" + input_size);

        var d3_form_input = d3_form_input_wrapper.append("input");
        d3_form_input.attr("id", machine_id + "_form_input");
        d3_form_input.attr("class", "form-control");
        d3_form_input.attr("type", "text");
        d3_form_input.attr("placeholder", placeholder);

        return d3_parent;
    };

    this.create_form_group = function(machine_id, man_readable_id, placeholder, label_size, input_size) {
        var d3_parent = this.get_parent(this.modal_id + "_fieldset");

        var d3_form_group = d3_parent.append("div");
        d3_form_group.attr("id", machine_id + "_form_group");
        d3_form_group.attr("class", "form-group");

        this.append_form_group_specifiy_sizes(d3_form_group, machine_id, man_readable_id, placeholder, label_size,
                input_size);
        return d3_form_group;

    };

    this.submit_form = function() {

    }
};