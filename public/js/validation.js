var Validation = function () {
	    this.isValidObject = function (object) {
	        return null !== object && undefined !== object;
	    };

		this.isValidString = function(object) {
			return this.isValidObject(object) && (typeof object === "string" || (typeof object === "object" && object.constructor === String)) && object.length > 0;
		};
		
		this.isValidNumber = function(object) {
			return this.isValidObject(object) && (!isNaN(parseFloat(object)) && isFinite(object));
		};
		
	    this.isValidArray = function (object) {
	        var result;

	        if (null === object || undefined === object) {
	            result = false;
	        } else if (typeof object === "object" && "splice" in object && "join" in object) {
	            result = true;
	        } else {
	            result = false;
	        }

	        return result;
	    };
};