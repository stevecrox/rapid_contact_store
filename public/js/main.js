var parent_panel_cell = "content-display-panel";
var create_store_parent = "create_store_parent_div";
var update_store_parent = "update_store_parent_div";
var table_parent = "contact_store_table_row_wrapper";

var client = new contact_store_rest_client();
var d3_create_modal = new contact_modal_ui_gen(parent_panel_cell, "create_store_modal");
var d3_update_modal = new contact_modal_ui_gen(parent_panel_cell, "update_store_modal");

$(document).ready(function() {

    var d3_parent = d3.select("#" + parent_panel_cell);

    // create a div wrapper for our create contact screen and then create a button to open it followed by a button
    var d3_create_parent_row = d3_parent.append("div");
    d3_create_parent_row.attr("class", "row");
    d3_create_parent_spacer = d3_create_parent_row.append("div");
    d3_create_parent_spacer.attr("class", "col-xs-1 col-sm-1 col-md-1 col-lg-1");

    d3_create_parent = d3_create_parent_row.append("div");
    d3_create_parent.attr("id", create_store_parent);
    d3_create_parent.attr("class", "pull-right col-xs-2 col-sm-2 col-md-2 col-lg-2");
    
    d3_create_parent_spacer_end = d3_create_parent_row.append("div");
    d3_create_parent_spacer_end.attr("class", "col-xs-1 col-sm-1 col-md-1 col-lg-1");

    // Create a button which will open our create contact screen
    d3_create_btn = d3_create_modal.create_modal_control_button(d3_create_parent, "create_store_modal_open_btn", "Create");
    d3_create_btn.attr("href", "#" + d3_create_modal.get_modal_parent_identifier());
    
    
    //
    var d3_table_row = d3_parent.append("div");
    d3_table_row.attr("id", "create_store_table_row");
    d3_table_row.attr("class", "row");
    
    d3_table_spacer = d3_table_row.append("div");
    d3_table_spacer.attr("class", "col-xs-1 col-sm-1 col-md-1 col-lg-1");

    d3_table_wrapper = d3_table_row.append("div");
    d3_table_wrapper.attr("id", table_parent);
    d3_table_wrapper.attr("class", "col-xs-10 col-sm-10 col-md-10 col-lg-10");
    
   
    client.getFieldData(build_gui);
});

function build_gui(header_names) {

    // Creates the modal contact creation form.
    d3_create_modal.generate_modal("Create a New Contact", "Create");

    var table_generator = new contact_table_ui_gen(table_parent, "store_table");
    table_generator.generate_table(header_names);
}
