var contact_store_rest_client = function(hostValue, portValue, contextValue, pathValue, protocolValue) {
	  
	  this.context="";
	  this.interfacePath="";
	  this.host=document.domain;
	  this.protocol=window.location.protocol;
	  this.port=window.location.port;
	  this.validator = new Validation();
	  
	  this.configure=function(hostValue, portValue, contextValue, pathValue, protocolValue) {
	      
	  };
	  
	  this.constructURL=function(path) {
		  var result = "";

		  if (this.validator.isValidString(this.protocol)) {
			  result += this.protocol + "//";
		  } else {
			  result += "http://";
		  }

		  if (this.validator.isValidString(this.host)) {
			  result += this.host;
		  } else {
			  result += "localhost";
		  }

		  if (this.validator.isValidNumber(this.port)) {
			  result += ":" + this.port;
		  }

		  if (this.validator.isValidString(this.context)) {
			  result += "/" + this.context;
		  }

		  if (this.validator.isValidString(this.interfacePath)) {
			  result += "/" + this.interfacePath;
		  }

		  if (this.validator.isValidString(path)) {
			  result += "/" + path;
		  }

		  return result;
	  };
	  
	  this.performQuery=function(path, callback) {
		  var url = this.constructURL(path);
		  
		  jQuery.ajax( url,{
                  type: 'GET',
                  accepts: 'json',
                  dataType: 'json',
                  success: function(data, textStatus, xhr) {
                	  callback(data);
                  },
                  error: function(xhr, textStatus, errorThrown) {
                      console.error("Rest interface: " + url + " failed");
                  }});
	  };

	  this.getFieldData=function(callback) {
		  this.performQuery("data/fields", callback);
	  };
	  
	  
	  this.configure(hostValue, portValue, contextValue, pathValue, protocolValue);
};